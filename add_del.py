import httplib2
import json
import sys


baseUrl = 'http://192.168.56.101:8080/controller/nb/v2'
containerName = 'default/'

def post_dict(h, url, d):
  resp, content = h.request(
      uri = url,
      method = 'PUT',
      headers={'Content-Type' : 'application/json'},
      body=json.dumps(d)
    )
  return resp, content

def post_flow(nodeid, new_flow, flowname):
    req_str = baseUrl + '/flowprogrammer/default/node/OF/' + nodeid + '/staticFlow/' + flowname    
    resp, content = post_dict(h, req_str, new_flow)

def build_flow(nodeid, flowname,flow_type, ethertype='', destip='', ipcos='', ipprot='', 
            installflag='', outnodeconn='', outdstmac='', vlan='', innodeconn=''):
    
    newflow = {}
    
    newflow['name'] = flowname
    if (installflag != ''):
        newflow['installInHw'] = installflag
    else:
        newflow['installInHw'] = 'true'
    newflow['node'] = {u'id': nodeid, u'type': u'OF'}
    if (destip != ''):
        newflow['nwDst'] = destip
    if (ethertype != ''):
        newflow['etherType'] = ethertype
    if (ipcos != ''):
        newflow['tosBits'] = ipcos
    if (ipprot != ''):
        newflow['protocol'] = ipprot
    if (vlan != ''):
        newflow['vlanId'] = vlan
    if (innodeconn != ''):
        newflow['ingressPort'] = innodeconn
    newflow['priority']=500
    node = {}
    node['id'] = nodeid
    node['type'] = 'OF'
    newflow['node'] = node

    if(flow_type == 1):    
        actions1 = 'OUTPUT='+str(outnodeconn)
        if (outdstmac != ''):
            actions2 = 'SET_DL_DST='+str(outdstmac)
        else:
            actions2 = ''
        newflow['actions'] = [actions1 + '','' + actions2]
    
    if(flow_type == 2):
        newflow['actions'] = ['DROP']
    
    return newflow

    
h = httplib2.Http(".cache")
h.add_credentials('admin', 'admin')

def delete_flow_node(node, flowname):
    del_url = 'http://192.168.56.101:8080/controller/nb/v2/flowprogrammer/default/node/OF/' + node + '/staticFlow/' + flowname
    resp, content = h.request(del_url, "DELETE")


if __name__ == "__main__":
     
    while 1:   
    
        print 'Flow addition'
        install = 'true'

        nodeid='00:00:00:00:00:00:00:02'
        nodeconnector=2
        ether_type=0x800
        dst_mac='00:00:00:00:00:02  '
        dst_ip='10.0.0.2'
        fname='flow1'
        print '1. Add flow'
        print '2. Add drop flow'
        print '3. Delete flow '
        choice = raw_input()
        if (choice == 1):

#           print 'Enter flow name'
#           fname = raw_input()
#           print 'Enter nodeid'
#           nodeid = raw_input()
#
#           print 'Enter node'
#           nodeconnector = raw_input()
#           print 'Dest. MAC'
#           dst_mac = raw_input()
#           print 'Dest. IP'
#           dst_ip = raw_input()


            new_flow = build_flow(nodeid=nodeid, flowname=fname,flow_type=choice, outnodeconn=nodeconnector, ethertype=ether_type, 
                            installflag = install, destip=dst_ip, outdstmac=dst_mac)
            print 'new_flow', new_flow
            post_flow(nodeid, new_flow, fname)

        if (choice == 2):
            new_flow = build_flow(nodeid=nodeid, flowname=fname,flow_type=choice, outnodeconn=nodeconnector, ethertype=ether_type, 
                            installflag = install, destip=dst_ip, outdstmac=dst_mac)
            print 'new_flow', new_flow
            post_flow(nodeid, new_flow, fname)

        if (choice == 3):
            print 'Enter Flow name'
            fname = raw_input()
            print 'Enter nodeid '
            node = raw_input()
            delete_flow_node(node =node, flowname=fname)
    
#    if (choice > 3):
#        print 'incorrect option'
#       break

  